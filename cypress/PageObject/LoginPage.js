class LoginPage {
    visit() {
        cy.visit("https://next-realworld.vercel.app/user/login")
    }

    getEmailField() {
        return cy.get(':nth-child(1) > .form-control')
    }

    getPasswordField() {
        return cy.get(':nth-child(2) > .form-control')
    }

    getButton() {
        return cy.get('.btn')
    }

    clickButton() {
        this.getButton().click()
    }

    urlVerify(expectedUrl) {
        cy.url().should('eq', expectedUrl)
    }

    errorMessageVerify(expectedMsg) {
        cy.get('.error-messages > li').should('contain', expectedMsg)
    }

    enterEmail(email) {
        this.getEmailField().type(email)
    }

    enterPassword(password) {
        this.getPasswordField().type(password)
    }
}

export default LoginPage
