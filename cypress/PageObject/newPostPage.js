class NewPostPage {

    visit() {
        cy.visit('https://next-realworld.vercel.app/editor/new')
    }

    fillTitle(title) {
        cy.get(':nth-child(1) > .form-control').type(title)
    }

    fillDescription(description) {
        cy.get(':nth-child(2) > .form-control').type(description)
    }

    fillBody(body) {
        cy.get(':nth-child(3) > .form-control').type(body)
    }

    fillTags(tags) {
        cy.get(':nth-child(4) > .form-control').type(tags)
    }

    submit() {
        cy.get('.btn').click()
    }
}

export default NewPostPage
