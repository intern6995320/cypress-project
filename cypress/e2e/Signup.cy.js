describe("Signup Test", () => {

    it('Successful Sign Up', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(1) > .form-control').type("roshan")
      cy.get(':nth-child(2) > .form-control').type("subediramesh177@gmail.com")
      cy.get(':nth-child(3) > .form-control').type("12345")
      cy.get('.btn').click()
    })
  
    it('Invalid email', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(1) > .form-control').type("roshan")
      cy.get(':nth-child(2) > .form-control').type("subediramesh177gmail.com")
      cy.get(':nth-child(3) > .form-control').type("12345")
      cy.get('.btn').click()
    })
  
    it('Already registered username', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(1) > .form-control').type("roshan")
      cy.get(':nth-child(2) > .form-control').type("subediramesh144@gmail.com")
      cy.get(':nth-child(3) > .form-control').type("12345")
      cy.get('.btn').click()
    })
  
    it('Already registered email', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(1) > .form-control').type("dipak")
      cy.get(':nth-child(2) > .form-control').type("subediramesh177@gmail.com")
      cy.get(':nth-child(3) > .form-control').type("12345")
      cy.get('.btn').click()
    })
  
    it('Missing email', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(1) > .form-control').type("tommy")
      cy.get(':nth-child(3) > .form-control').type("7894")
      cy.get('.btn').click()
    })
  
    it('Missing password', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(1) > .form-control').type("tam12@gmail.com")
      cy.get(':nth-child(2) > .form-control').type("tam")
      cy.get('.btn').click()
    })
  
    it('Weak password', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(1) > .form-control').type("bonny")
      cy.get(':nth-child(2) > .form-control').type("bonny1@gmail.com")
      cy.get(':nth-child(3) > .form-control').type("4567")
      cy.get('.btn').click()
    })
  
    it('Missing username', () => {
      cy.visit("https://next-realworld.vercel.app/user/register")
      cy.get(':nth-child(2) > .form-control').type("bonny1@gmail.com")
      cy.get(':nth-child(3) > .form-control').type("4567")
      cy.get('.btn').click()
    })
  
  })
  