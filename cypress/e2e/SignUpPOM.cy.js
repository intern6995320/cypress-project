import signUpPage from "../PageObject/signUpPage"

describe("Signup Test", () => {
    
    const signUpPageInstance = new signUpPage()

    beforeEach(() => {
        signUpPageInstance.visit()
    })

    it("Successful Sign Up", () => {
        signUpPageInstance.enterUsername("mahesh")
        signUpPageInstance.enterEmail("subediramesh11@gmail.com")
        signUpPageInstance.enterPassword("12345")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyPageElementVisibility()
    })

    it("Invalid email", () => {
        signUpPageInstance.enterUsername("roshan")
        signUpPageInstance.enterEmail("subediramesh111gmail.com")
        signUpPageInstance.enterPassword("12345")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyUrl('https://next-realworld.vercel.app/user/register')
    })

    it("Already registered username", () => {
        signUpPageInstance.enterUsername("roshan")
        signUpPageInstance.enterEmail("subediramesh144@gmail.com")
        signUpPageInstance.enterPassword("12345")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("username has already been taken")
    })

    it("Already registered email", () => {
        signUpPageInstance.enterUsername("dipak")
        signUpPageInstance.enterEmail("subediramesh177@gmail.com")
        signUpPageInstance.enterPassword("12345")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("email has already been taken")
    })

    it("Missing email", () => {
        signUpPageInstance.enterUsername("tommy")
        signUpPageInstance.enterPassword("7894")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("email can't be blank")
    })

    it("Missing password", () => {
        signUpPageInstance.enterUsername("hape")
        signUpPageInstance.enterEmail("tam12@gmail.com")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("password can't be blank")
    })

    it("Weak password", () => {
        signUpPageInstance.enterUsername("bobb")
        signUpPageInstance.enterEmail("bonny123@gmail.com")
        signUpPageInstance.enterPassword("4567")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("password is too short")
    })

    it("Missing username", () => {
        signUpPageInstance.enterEmail("bonny1111@gmail.com")
        signUpPageInstance.enterPassword("4567")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("username can't be blank")
    })

    it("Sign Up with password under 8 characters", () => {
        signUpPageInstance.enterUsername("roshan")
        signUpPageInstance.enterEmail("subediramesh189@gmail.com")
        signUpPageInstance.enterPassword("123456")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("password is too short")
    })

    it("Sign Up with password over 14 characters", () => {
        signUpPageInstance.enterUsername("rosh")
        signUpPageInstance.enterEmail("subediramesh1196@gmail.com")
        signUpPageInstance.enterPassword("averylongpassword")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("password is too long")
    })

    it("Sign Up with email address without '@'", () => {
        signUpPageInstance.enterUsername("raspberryjam")
        signUpPageInstance.enterEmail("raspberryja.com")
        signUpPageInstance.enterPassword("raspberryjam")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyUrl('https://next-realworld.vercel.app/user/register')
    })

    it("Sign Up with invalid email domain", () => {
        signUpPageInstance.enterUsername("raspberyjam")
        signUpPageInstance.enterEmail("raspberr@jam")
        signUpPageInstance.enterPassword("raspberryjam")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyUrl('https://next-realworld.vercel.app/user/register')
    })

    it("Sign Up with password over 8 characters and under 15 characters", () => {
        signUpPageInstance.enterUsername("rasberryjam")
        signUpPageInstance.enterEmail("raspberr1@jam.com")
        signUpPageInstance.enterPassword("raspberry")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyPageElementVisibility()
    })

    it("Sign Up with special characters in username", () => {
        signUpPageInstance.enterUsername("congratulation!")
        signUpPageInstance.enterEmail("kangaroo1@gmail.com")
        signUpPageInstance.enterPassword("gooood")
        signUpPageInstance.clickButton()
        signUpPageInstance.verifyErrorMessage("username is invalid")
    })
})

