import NewPostPage from '../PageObject/newPostPage'

describe('New Post Module', () => {
    const newPostPage = new NewPostPage()

    beforeEach(() => {
        // Visit the homepage
        cy.visit('https://next-realworld.vercel.app/')
        
        // Navigate to login page and perform login
        cy.get(':nth-child(2) > .nav-link').click() // Click login link
        cy.get(':nth-child(1) > .form-control').type('abcde@gmail.com') // Type email
        cy.get(':nth-child(2) > .form-control').type('23456') // Type password
        cy.get('.btn').click() // Submit login form

        // Visit new post page
        newPostPage.visit()
    })

    it('should post successfully', () => {
        newPostPage.fillTitle('Ramesh')
        newPostPage.fillDescription('Life story of Ramesh')
        newPostPage.fillBody('Its me Ramesh. I am from Morang, Nepal. And I am a good boy')
        newPostPage.fillTags('#goodboy')
        newPostPage.submit()
    })
})
