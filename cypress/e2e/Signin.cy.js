describe('sign in module', () =>{
    beforeEach(() =>{
        cy.visit('https://next-realworld.vercel.app/')
        cy.get(':nth-child(2) > .nav-link').click()
    })

        it('sucessfully sign in with valid credentials',()=>{

        //cy.visit('https://next-realworld.vercel.app/')
        //cy.get(':nth-child(2) > .nav-link').click()
        cy.get(':nth-child(1) > .form-control').type('abcde@gmail.com')
        cy.wait(500)
        cy.get(':nth-child(2) > .form-control').type('23456')
        cy.get('.btn').click()

    })
    it('signin unsuccessful with invalid credentials', ()=>{
        //cy.visit('https://next-realworld.vercel.app/')
        //cy.get(':nth-child(2) > .nav-link').click()
        cy.get(':nth-child(1) > .form-control').type('abc@gmail.com')
        cy.wait(500)
        cy.get(':nth-child(2) > .form-control').type('2456')
        cy.get('.btn').click()        

    })
    it('signin unsuccessful with invalid email', () => {
        cy.get(':nth-child(1) > .form-control').type('mnopgmail.com')
        cy.get(':nth-child(2) > .form-control').type('23456')
        cy.get('.btn').click()
        
    })

    it('signin unsuccessful with invalid password', () => {
        cy.get(':nth-child(1) > .form-control').type('abcde@gmail.com')
        cy.get(':nth-child(2) > .form-control').type('999999')
        cy.get('.btn').click()
        
    })

    it('signin unsuccessful with missing email', () => {
        cy.get(':nth-child(2) > .form-control').type('23456')
        cy.get('.btn').click()
       
    })

    it('signin unsuccessful with missing password', () => {
        cy.get(':nth-child(1) > .form-control').type('abcde@gmail.com')
        cy.get('.btn').click()
       
    })

    it('signin unsuccessful with uppercase letter in registered email', () => {
        // Assuming the email 'abcde@gmail.com' is registered with lowercase letters only
        cy.get(':nth-child(1) > .form-control').type('ABCDE@gmail.com')
        cy.get(':nth-child(2) > .form-control').type('23456')
        cy.get('.btn').click()
        
    })
})


