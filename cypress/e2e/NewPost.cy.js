describe('signup module', ()=>{
    beforeEach( ()=>{
        cy.visit('https://next-realworld.vercel.app/')
        cy.get(':nth-child(2) > .nav-link').click()
        cy.get(':nth-child(1) > .form-control').type('abcde@gmail.com')
        cy.get(':nth-child(2) > .form-control').type('23456')
        cy.get('.btn').click()
        cy.visit('https://next-realworld.vercel.app/editor/new')
    })
    it('should post successfully', ()=>{
        cy.get(':nth-child(1) > .form-control').type('Ramesh')
        cy.get(':nth-child(2) > .form-control').type('Life story of Ramesh')
        cy.get(':nth-child(3) > .form-control').type('Its me Ramesh. I am from Morang,Nepal. And i am a good boy')
        cy.get(':nth-child(4) > .form-control').type('#goodboy')
        cy.get('.btn').click()
    })

})