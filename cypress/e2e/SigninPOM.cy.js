import LoginPage from '../PageObject/LoginPage'

describe("Login with POM", () => {
    const loginPage = new LoginPage()

    beforeEach(() => {
        loginPage.visit()
    })

    it('Sign In with valid credentials', () => {
        loginPage.enterEmail('abcde@gmail.com')
        loginPage.enterPassword('23456')
        loginPage.clickButton()
        loginPage.urlVerify("https://next-realworld.vercel.app/") 
    })

    it('Sign In with invalid credentials', () => {
        loginPage.enterEmail('abc@gmail.com')
        loginPage.enterPassword('2456')
        loginPage.clickButton()
        loginPage.errorMessageVerify('email or password is invalid') 
    })

    it('Sign In with invalid email', () => {
        loginPage.enterEmail('abcgmail.com')
        loginPage.enterPassword('23456')
        loginPage.clickButton()
        loginPage.urlVerify("https://next-realworld.vercel.app/user/login") 
    })

    it('Sign In with invalid password', () => {
        loginPage.enterEmail('abcde@gmail.com')
        loginPage.enterPassword('99999')
        loginPage.clickButton()
        loginPage.errorMessageVerify('email or password is invalid') 
    })

    it('Sign In with empty email field', () => {
        loginPage.enterPassword('23456')
        loginPage.clickButton()
        loginPage.errorMessageVerify('email can\'t be blank') 
    })

    it('Sign In with empty password field', () => {
        loginPage.enterEmail('abcde@gmail.com')
        loginPage.clickButton()
        loginPage.errorMessageVerify('password can\'t be blank') 
    })

    it('Sign In with registered user account using Uppercase email input', () => {
        loginPage.enterEmail('ABCDE@gmail.com')
        loginPage.enterPassword('23456')
        loginPage.clickButton()
        loginPage.urlVerify("https://next-realworld.vercel.app/user/login") 
    })
})
