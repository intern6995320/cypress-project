describe('sign in module', () => {

    beforeEach(() => {
        cy.visit('https://next-realworld.vercel.app/')
        cy.get(':nth-child(2) > .nav-link').click()
    })

    it('successfully sign in with valid credentials', () => {
        cy.fixture('credentials.json').then(credentials => {

            const jsonData = credentials.valid_credentials
            cy.get(':nth-child(1) > .form-control').type(jsonData.email)
            cy.get(':nth-child(2) > .form-control').type(jsonData.password)
            cy.get('.btn').click()
           
        })
    })
     it('unsuccessful sign in with invalid credentials', () => {
           cy.fixture('credentials.json').then(credentials => {
            const jsonData = credentials.invalid_credentials
              cy.get(':nth-child(1) > .form-control').type(jsonData.email)
              cy.get(':nth-child(2) > .form-control').type(jsonData.password)
             cy.get('.btn').click()
           
        })
        })

     it('signin unsuccessful with invalid email', () => {
        cy.fixture('credentials.json').then(credentials => {
            const jsonData = credentials.invalid_email
            cy.get(':nth-child(1) > .form-control').type(jsonData.email)
            cy.get(':nth-child(2) > .form-control').type(jsonData.password)
             cy.get('.btn').click()
            
         })
     })

     it('signin unsuccessful with invalid password', () => {
         cy.fixture('credentials.json').then(credentials => {
            const jsonData = credentials.invalid_password
             cy.get(':nth-child(1) > .form-control').type(jsonData.email)
             cy.get(':nth-child(2) > .form-control').type(jsonData.password)
             cy.get('.btn').click()
          
         })
     })

          it('signin unsuccessful with missing email', () => {
         cy.fixture('credentials.json').then(credentials => {
            const jsonData = credentials.missing_email
             cy.get(':nth-child(2) > .form-control').type(jsonData.password)
            cy.get('.btn').click()
        
        })
    })

    it('signin unsuccessful with missing password', () => {
        cy.fixture('credentials.json').then(credentials => {
            const jsonData = credentials.missing_password
            cy.get(':nth-child(1) > .form-control').type(jsonData.email)
            cy.get('.btn').click()
           
        })
    })

    it('signin unsuccessful with uppercase letter in registered email', () => {
        cy.fixture('credentials.json').then(credentials => {
            const jsonData = credentials.uppercase_email
            cy.get(':nth-child(1) > .form-control').type(jsonData.email)
            cy.get(':nth-child(2) > .form-control').type(jsonData.password)
            cy.get('.btn').click()
           
        })
    })
})
